# Universidad Distrital Francisco José de Caldas

## DISEÑO ARQUITECTURAL DE SOFTWARE Y PATRONES

## Miembros del grupo

- Karen Milena Pinilla - 
- Edwar Díaz Ruiz - 20141020004
- Daissi Bibiana Gonzales Roldan - 20152020108
- Gabriel Vargas Monroy - 20141020107

### Modelo de Componentes
![modelo de componentes](ImagenesDiagramas/componentes.png)

### Modelo de Paquetes

#### Principal
![modelo de paquetes principal](ImagenesDiagramas/paquetes_principal.png)

#### Logica
![modelo de paquetes.logica](ImagenesDiagramas/paquetes_logica.png)

#### Estudiante
![modelo de paquetes.logica.estudiante](ImagenesDiagramas/paquetes_logica_estudiante.png)

#### Docente
![modelo de paquetes.logica.docente](ImagenesDiagramas/paquetes_logica_docente.png)

#### Materia
![modelo de paquete.logica.materia](ImagenesDiagramas/paquetes_logica_materia.png)

### Modelo de Secuencia

#### Colocar Notas
![modelo de secuencia colocar notas](ImagenesDiagramas/ColocarNotas.png)

#### Autenticacion
![modelo de secuencia logging](ImagenesDiagramas/loging.png)

#### Ver Notas
![modelo de secuencia ver notas](ImagenesDiagramas/VerNotas.png)

### Modelo de Estado

#### Colocar Notas
![modelo de estado colocar notas](ImagenesDiagramas/Estados-ColocarNotas.png)

#### Autenticacion
![modelo de estado logging](ImagenesDiagramas/Estados-Login.png)

#### Ver Notas
![modelo de estado ver notas](ImagenesDiagramas/Estados-VerNotas.png)

### Modelos
- Modelo de componentes
- Modelo de paquetes
- Modelo de estados
- Modelo de Secuencia/Colaboracion
